import React, { Component } from 'react'
import {Redirect} from "react-router-dom"
import Header from "./Header"
import List from "./List"

import "../App.css"

export default class Hobby extends Component {
    constructor(){
        super()
        this.state ={
            value:"",
            userID:"",
            email:"",
            number:"",
            redirect:false,
            hobbies:[],
            errors:[]
            }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.HandelChange = this.HandelChange.bind(this)
    this.isLogout = this.isLogout.bind(this)
    this.handleDelete =this.handleDelete.bind(this)
    }

    componentDidMount(){

        const localData = localStorage.getItem("userData")
         if( localData === null ){
            return this.setState({ redirect:true}) 
         }
          
        const parsedData = JSON.parse(localData)
        console.log(parsedData._id)
        this.setState({
            userID : parsedData._id,
            email:parsedData.email,
            number:parsedData.number
        })
        
        fetch("api/hobby").then((json)=>{
            return json.json()
        }).then((data)=>{
            console.log(data)
            this.setState({
                hobbies:data.data
            })
        }).catch((err)=>{
            console.log(err)
        })
    }

    showValidationMsg(elm,msg){
        return this.setState((prevState)=>({
            errors:[...prevState.errors,{elm,msg}]
        }))
    }

    clearError(elm){
        console.log(elm)
        
        this.setState((prevState)=>{
            let arr = []
            for(let err of prevState.errors){
                if(elm !== err.elm){
                  arr.push(err)
                }
            }
            return {errors:arr};
        })
        
     
    }

    HandelChange(event){
        const {name,value} = event.target
        this.setState({
         [name]:value
        })
        this.clearError(name)
        // if(value.trim() === ""){
        //     return this.showValidationMsg("value","You have not entered a hobby ")
        //       }
    }
   
    handleSubmit(event){
        event.preventDefault();
          if(this.state.value.trim() === ""){
           return this.showValidationMsg("value","You have not entered a hobby ")
          }
          
      const data ={
          value : this.state.value,
          userID : this.state.userID,
          number:this.state.number,
          email:this.state.email
      }

      console.log(data)
      const obj =JSON.stringify(data)
     

      fetch("api/hobby",{
          method:"POST",
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
          body:obj
        }).then(response =>  response.json())
        .then(data =>{
            console.log(data)
            this.setState({hobbies: this.state.hobbies.concat(data.data)})
        })
        .catch((err)=>{console.log(err) })
      

    }

    isLogout(){
        console.log("ok")
          localStorage.clear()
        this.setState({ redirect:true})
    }
    handleDelete(id){
     fetch("api/hobby/" + id, {
         method:"DELETE"
     } )
     .then(res=> res.json())
     .then(data =>{
         if(data.success){
            
         }
         })

    }
    // static contextType = RegisterContext;              
    render() {
       let noHobby = null;
       for(let err of this.state.errors){
       if(err.elm === "value"){
           noHobby = err.msg
       }
       }
        const fromHobby = this.state.hobbies.filter((hobby)=>{
           
            return hobby.userID === this.state.userID
        })
        
        const filtered = fromHobby.map((hobby,index)=>{
            return <List key={index} hobby={hobby} handleDelete ={this.handleDelete}/>
        })
        
        if(filtered.length === 0){
            return (
                <>
                {this.state.redirect ? <Redirect to="/" /> : "" }
               <Header isLogout ={this.isLogout}/>
                <div className="section-1">
                    <section className="content">
                       
                    <div className="section-div">
                        <div className="no-hobby">
                            <div className={noHobby ? "noHobby" :"hide"}>{noHobby ? noHobby :""}</div>
                            <p>You don't have a hobby yet</p>
                        </div>
    
                        <div className="form-input">
                            <form className="form-group" onSubmit={this.handleSubmit}>
                                <div className="form-field">
                                <input type="text" name="value"  className="form-control" placeholder="hobby"  onChange={this.HandelChange}/>
                                </div>
                                <div className="btn-hobby">
                                   <button className="btn-primary form-control">Add</button>
                                </div>
                                
                            </form>
                        </div>
                    </div>
                    
                    </section>
                </div>
                
                </>
            )

        }else{
            return (
                <>
                {this.state.redirect ? <Redirect to="/" /> : "" }
               <Header isLogout ={this.isLogout}/>
                <div className="section-1">
                    <section className="content">
                        <div className="headding">
                        <div className={noHobby ? "noHobby" :"hide"}>{noHobby ? noHobby :""}</div>
                        <h1 className="title">I fell good</h1>
                        </div>
                        
                    <div className="section-div">
                       <div>
                            {filtered}
                       </div>
    
                        <div className="form-input">
                            <form className="form-group" onSubmit={this.handleSubmit}>
                                <div className="">
                                   <input type="text" name="value"  className="form-control" placeholder="hobby"  onChange={this.HandelChange}/>
                                </div>
                                <div className="btn-hobby">
                                   <button className="btn-primary form-control">Add</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    
                    </section>
                </div>
                
                </>
            )
        }
        
    }
}
