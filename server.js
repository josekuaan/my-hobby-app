const express=require("express")
const cors = require("cors")
const bodyParser = require("body-parser")
const morgan = require("morgan") 
 require('dotenv').config()
const twilio = require('twilio')
const sgMail = require('@sendgrid/mail')

 sgMail.setApiKey(process.env.SENDGRID_API_KEY);


 const TWILIO_ACCOUNT_SID = process.env.TWILIO_ACCOUNT_SID;
 const TWILIO_AUTH_TOKEN = process.env.TWILIO_AUTH_TOKEN;
 const TWILIO_PHONE_NUMBER = process.env.TWILIO_PHONE_NUMBER
 const client = new twilio(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN);

         

  

/// local import
var {mongoose} = require('./db/mongoose')
const {User} = require("./models/users")
const {Hobby} = require("./models/hobby")

const API_PORT =4000
const app = express()
    app.use(cors())

    const router = express.Router()



//bodyparser parses the request to a readable json format

app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())
// app.use(logger('div'))

//getting all data from db

router.get("/hobby",(req,res)=>{
     Hobby.find((err,data)=>{
         if(err){res.json({
             success:false,error:err})
             return;
            }
         res.send({success:true,data:data})
     })
})

router.post("/hobby",(req,res)=>{

    const {value,email,number,userID} = req.body
    

    if( !value || !email || !number || !userID ){
       return  res.json({
            success:false,
            error: 'INVALID INPUT'
        })
    }
   
    console.log(value,email,number,userID)
    const hobbies = new Hobby()
    hobbies.text = value;
    hobbies.userID = userID;
    hobbies.save().then((data)=>{

       return  res.send({success:true,data:data})
    }).catch((err)=>{
       return  res.json({success:false,error:err})
    })


    ///
 
  
    client.messages.create({
     body:  value,
     from: TWILIO_PHONE_NUMBER,
     to: '+234' + number
   })
  .then(()=>{
     return  res.send(JSON.stringify({success:true}));
  })
  .catch(err =>{
      
    return res.send(JSON.stringify({success:false,message:err}))
})


//twilio ends
///////////////////////
//sendgrid here  
//////////////////////

const msg = {
    to:  email,
    from: 'test@example.com',
    subject: 'Hobby added',
    text:  value
    
  };
   
 sgMail.send(msg)
//sendgrid ends
});


router.post("/register",(req,res)=>{

    const {username,email,password,number} = req.body
    console.log(username,email,password,number)

    if( !username || !email || !password || !number){
        res.json({
            success:false,
            error: 'INVALID INPUT'
        })
    }
    const user = new User()
    
    user.username = username
    user.email = email
    user.password= password
    user.number= number

    user.save().then((data)=>{
      
       return  res.send({success:true,userData:data})
    }).catch((err)=>{
        res.json({success:false,error:err})
    })

})
router.post("/login",(req,res)=>{

    const {email,password} = req.body

    if( !email || !password){
        res.json({
            success:false,
            error: 'INVALID INPUT'
        })
    }
    
 User.authenticate(email,password).then((user)=>{

return res.send({success:true,userData:user})
     
 }).catch(()=>{
     res.send({success:false,message:"INVALID Email or Password"})
 })


})
router.delete("/hobby/:id",(req,res)=>{
    
    Hobby.findByIdAndRemove({ _id: req.params.id }).then((hobby)=>{
        res.send({success:true})
      }).catch(()=>{
      res.send({success:false,message:"Could Not delete hobby"})
  })
})
app.use("/api",router)


if (process.env.NODE_ENV === 'production') {
    // Serve any static files
    app.use(express.static(path.join(__dirname, '../client/build')));
      
    // Handle React routing, return all requests to React app
    app.get('*', function(req, res) {
      res.sendFile(path.join(__dirname, '..client/build', 'index.html'));
    });
  }

//this is for http



app.listen(API_PORT,()=>{console.log(`server is live on port > ${API_PORT}`)})

