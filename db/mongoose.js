const mongoose = require("mongoose")
require('dotenv').config()

mongoose.Promise = global.Promise;

const CLUSTER_URL = process.env.CLUSTER_URL
console.log(CLUSTER_URL)
const dbRoute = "mongodb://localhost:27017/hobbyProject"
 const url = process.env.NODE_ENV === "production" ? CLUSTER_URL : dbRoute;

mongoose.connect(url,{useNewUrlParser:true,useCreateIndex: true,useFindAndModify:false})

module.exports = {mongoose}