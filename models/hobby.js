const mongoose =require("mongoose")

const  HobbySchema = new mongoose.Schema({
    
    text:{
    type:String,
    required:true,
    trim:true
   },
   userID:{
       type:String,
       index:true,
       sparse:true,
       required:true
   },
   createdAt : { type : Date, default: Date.now },
   updatedAt : { type : Date, default: Date.now }
    
})

Hobby = mongoose.model('Hobby', HobbySchema);
module.exports = {Hobby};