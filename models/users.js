const mongoose =require("mongoose")
const bcrypt = require("bcrypt")

const UserSchema = new mongoose.Schema({
    
    id:{
    type:Number
    },
    username:{
    type:String,
    required:true,
    trim:true
   },
    email:{type:String,
    required:true,
    trim:true
    },
    password:{
        type:String,
        required:true,
        trim:true
     },
     number:{
        type:String,
        required:true,
        trim:true
     }
    

})

//hashing a password before saving it to the database
UserSchema.pre('save', function (next) {
    var user = this;
    bcrypt.hash(user.password, 10, function (err, hash){
      if (err) {
        return next(err);
      }
      user.password = hash;
      next();
    })
  });

  //authenticate input against database
UserSchema.statics.authenticate = function (email, password) {
  var User= this
    return User.findOne({ email: email })
      .then(function ( user) {
        // console.log( user)
         if (!user) {
          return Promise.reject()
        }
        return new Promise((resolve,reject)=>{
          bcrypt.compare(password, user.password, function (err, result) {
          console.log(result)
          if (result) {
            return resolve( user);
          } else {
            return reject(); 
          }
        })
        })
        
      });
  }
User = mongoose.model('User', UserSchema);

module.exports = {User}